# README #

### Recursos utilizados ###
* Código proveniente de https://www.agnosticdev.com/content/how-use-commoncrypto-apis-swift-5  para a geração de chaves de MD5
*  Como splashscreen e como icon da aplicação foi utilizada a imagem de Elijah O'Donnell on Unsplash
* Para a geração dos icons foi utilizada a ferramenta -> https://appicon.co
*  Imagem dos favoritos foram utilizadas as seguintes:
* Favorite icon -> <div>Icons made by <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

    <div>Icons made by <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>


### Breve explicação ###

* Durante o processo de desenvolvimento foi utilizada uma arquitectura MVC. Foi criada uma classe para fazer o download dos dados e correcta passagem para o Model. 
* Foi criado um viewController para o ecrã de listagem e outro para o ecrã de detalhes. 
* A pesquisa pelo herói da marvel recorre ao mesmo pedido para obter os dados utilizada para preencher a listagem, utilizado apensas adicionalmente o nome inserido como parâmetro. 
* Por limitação de tempo não foi possível criar uma animação personalizada nem carregar a descrição dos comics, stories, events e series (séria necessário um novo request para obter estas informações) 
* Adicionalmente foram criadas traduções (app em multi-language Português e Inglês), adicionados testes unitários ( apenas como ilustração devido a falta de tempo) e validação  de excesso de pedidos a api. 


### Tempo de desenvolvimento ###

Tempo alocado ao desenvolvimento cerca de 16 horas
