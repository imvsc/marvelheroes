//
//  DownloadMarvelHeroes.swift
//  MarvelHeroes
//
//  Created by Isabel Couto on 14/04/2020.
//  Copyright © 2020 Isabel Couto. All rights reserved.
//

import UIKit
import Foundation
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG


protocol DownloadMarvelHeroesDelegate {
    func didComplete(result: MarvelHeroes)
    func onErrorResult()
}

class DownloadMarvelHeroes: NSObject {
    static var url = "https://gateway.marvel.com:443/v1/public/"
    static var publicKey = "c703253d2b6eacb193b62775ab7f75e5"
    static var privateKey = "1d50dd434b57a1b133379dfbc8122165c4ff86a9"
    var delegate: DownloadMarvelHeroesDelegate?

    
    static func fetchMarvelHeroes(delegate:DownloadMarvelHeroesDelegate,offset:String,searchfilterName:String){
       
        
        let timeStamp = Int64(Date().timeIntervalSince1970 * 1000).description
        
        let str = timeStamp + privateKey + publicKey
        
        let hash = md5Hash(str: str )
        var search = ""
        if searchfilterName.count>0 {
            search = "&nameStartsWith=" + searchfilterName.replacingOccurrences(of: " ", with: "%20")
        }
        let urlString =  url + "/characters?ts=" + timeStamp + search + "&limit=20" + "&offset=" + offset + "&apikey=" + publicKey + "&hash=" + hash;
        
        
        
        let request = URLRequest(url: URL.init(string: urlString)!)
        
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            _ = (response as? HTTPURLResponse)?.statusCode ?? 200
            
            do {
                let marvelHeroes = try JSONDecoder().decode(MarvelHeroes.self, from: data!)
                delegate.didComplete(result: marvelHeroes)
            }
            catch {
               print(error)
                delegate.onErrorResult()
            }
           
        }
        dataTask.resume()

        
    }
    
    
    // https://www.agnosticdev.com/content/how-use-commoncrypto-apis-swift-5
    static func md5Hash (str: String) -> String {
        if let strData = str.data(using: String.Encoding.utf8) {
            /// #define CC_MD5_DIGEST_LENGTH    16          /* digest length in bytes */
            /// Creates an array of unsigned 8 bit integers that contains 16 zeros
            var digest = [UInt8](repeating: 0, count:Int(CC_MD5_DIGEST_LENGTH))
            
            /// CC_MD5 performs digest calculation and places the result in the caller-supplied buffer for digest (md)
            /// Calls the given closure with a pointer to the underlying unsafe bytes of the strData’s contiguous storage.
            strData.withUnsafeBytes {
                // CommonCrypto
                // extern unsigned char *CC_MD5(const void *data, CC_LONG len, unsigned char *md) --|
                // OpenSSL                                                                          |
                // unsigned char *MD5(const unsigned char *d, size_t n, unsigned char *md)        <-|
                CC_MD5($0.baseAddress, UInt32(strData.count), &digest)
            }
            
            
            var md5String = ""
            /// Unpack each byte in the digest array and add them to the md5String
            for byte in digest {
                md5String += String(format:"%02x", UInt8(byte))
            }
            return md5String
            
        }
        return ""
    }
}
