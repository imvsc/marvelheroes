//
//  ViewController.swift
//  MarvelHeroes
//
//  Created by Isabel Couto on 13/04/2020.
//  Copyright © 2020 Isabel Couto. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var hasMoreToDownload:Bool = true;
    private var list:[MarvelHeroe]?
    private var searchList:[MarvelHeroe]?
    private var isSearching:Bool = false
    @IBOutlet weak var searchBar: UISearchBar!
   
    
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        list = []
        searchList = []
        DownloadMarvelHeroes.fetchMarvelHeroes(delegate: self, offset: "0", searchfilterName: "")
        self.navigationBar.topItem?.title = NSLocalizedString("TitleViewController", comment: "")
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }

}

extension ViewController:DownloadMarvelHeroesDelegate{
    func didComplete(result: MarvelHeroes) {
        if(result.data==nil) {
            DispatchQueue.main.async() {    // execute on main thread
                let alert = UIAlertController(title: NSLocalizedString("ErrorApiTitle", comment: ""), message: NSLocalizedString("ErrorApiMessage", comment:""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("ErrorApiRetry", comment: ""), style: .default, handler:  { action in
                    self.getMoreData()
                }))
                self.present(alert, animated: true)
            }
            return
        }
        if(result.data!.offset < result.data!.total){
            hasMoreToDownload = true;
        }else{
            hasMoreToDownload = false;
        }
        if(isSearching){
            searchList = result.data!.results
        }else{
            list?.append(contentsOf: result.data!.results)

        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func onErrorResult() {
        print("error")
    }
    
    func getMoreData(){
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async{
            let offset:String = self.list?.count.description ?? "0"
            DownloadMarvelHeroes.fetchMarvelHeroes(delegate: self, offset: offset, searchfilterName: "")
        }
       
    }
    
    
}

//
// MARK: - Table View Data Source
//
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (isSearching ? (searchList?.count ?? 0) : (list?.count ?? 0))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let marvelHerroCell = tableView.dequeueReusableCell(withIdentifier: "MarvelHeroCell", for: indexPath) as! TableViewCellMarvelHero
        var marvel:MarvelHeroe
        if isSearching {
           marvel = (searchList?[indexPath.row])!
        }else{
            marvel = (list?[indexPath.row])!
            
        }
       marvelHerroCell.loadCell(marvelHero: marvel)
        
        return marvelHerroCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MarvelHero") as? ViewControllerMarvelHeroe
        vc?.marvelHero = (isSearching ? self.searchList![indexPath.row] as MarvelHeroe : self.list![indexPath.row] as MarvelHeroe)
        present(vc!, animated: true, completion: nil)
        
    }
    
    

}

extension ViewController:UITableViewDelegate{
      func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell,forRowAt indexPath: IndexPath){
        if !isSearching &&  hasMoreToDownload && indexPath.row + 3 ==  (isSearching ? searchList!.count : list?.count) {
                getMoreData()
            }
    }
}

extension ViewController:UISearchBarDelegate{
     func searchBar(_ searchBar: UISearchBar,textDidChange searchText: String){
        if(isSearching){
            if(searchText.count>0){
                print("Continue searching");
                searchList = []
                DownloadMarvelHeroes.fetchMarvelHeroes(delegate: self, offset: "0", searchfilterName: searchText)

            }else{
                isSearching = false;
                searchList = []
                list = []
                print("Stop search");
                DownloadMarvelHeroes.fetchMarvelHeroes(delegate: self, offset: "0", searchfilterName: "")
            }
        }else{
            isSearching = true;
            print("Star search");
        }
    }
}

