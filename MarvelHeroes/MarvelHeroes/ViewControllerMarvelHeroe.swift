//
//  ViewControllerMarvelHeroe.swift
//  MarvelHeroes
//
//  Created by Isabel Couto on 19/04/2020.
//  Copyright © 2020 Isabel Couto. All rights reserved.
//

import UIKit

class ViewControllerMarvelHeroe: UIViewController {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var marvelHeroImage: UIImageView!
    
    @IBOutlet weak var navigationBarItem: UINavigationItem!
    
    @IBOutlet weak var favoriteButton: UIBarButtonItem!
    
    var rightButton:UIButton?
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    var marvelHero:MarvelHeroe?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
       self.navigationBarItem.leftBarButtonItem = UIBarButtonItem.init(title: NSLocalizedString("BackButton", comment: ""), style: .done, target: self, action:#selector(backAction(sender:)))
        
        rightButton = UIButton(frame:CGRect.init(x: 0, y: 0, width: 50, height: 50))
        rightButton?.addTarget(self, action:#selector(addToFavorite(sender:)), for: .touchUpInside)
        updateFavorite()
        self.navigationBarItem.rightBarButtonItem = UIBarButtonItem.init(customView: rightButton!)
        self.favoriteButton = UIBarButtonItem.init(customView: rightButton!)
           self.navigationBarItem.title = self.marvelHero?.name
            
            let url = URL(string: (self.marvelHero?.thumbnail.path)! + "." + (self.marvelHero?.thumbnail.extension)!)
            let imageDownload :URLSessionDataTask = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async() {    // execute on main thread
                    self.marvelHeroImage?.image = UIImage(data: data)
                    self.marvelHeroImage?.stopAnimating()
                    
                }
            }
            
            imageDownload.resume()
            self.prepareText()
    
    }
    
   
    
    @objc func backAction(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func prepareText(){
        let textView = UITextView(frame:CGRect(x: 0, y: 20, width: scrollView.frame.size.width, height: 0))
        let font =  UIFont(name:"HelveticaNeue-Bold", size: 16.0)

        let firstAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        let firstString = NSMutableAttributedString(string: NSLocalizedString("Comics", comment: "") + " \n ", attributes: firstAttributes)
        var auxComicString  = ""
        guard let arrayComics =  marvelHero?.comics.items.prefix(3) else { return    }
        for comic in arrayComics{
            auxComicString = auxComicString + comic.name + "\n"
        }
        
        
        let secondString = NSAttributedString(string: auxComicString , attributes: secondAttributes)
        let thirdString = NSAttributedString(string: NSLocalizedString("Events", comment: "") + " \n ",attributes: firstAttributes)
        
        var auxEventsString  = ""
        guard let arrayEvents =  marvelHero?.events!.items.prefix(3) else {  return  }
        for events in arrayEvents{
            auxEventsString = auxEventsString + events.name + "\n"
        }
        let fourthString = NSAttributedString(string: auxEventsString , attributes: secondAttributes)

        let fifthString = NSAttributedString(string: NSLocalizedString("Stories", comment: "") + " \n ",attributes: firstAttributes)
        
        var auxStoriesString  = ""
        guard let arrayStories =  marvelHero?.stories!.items.prefix(3) else {  return }
        for stories in arrayStories{
            auxStoriesString = auxStoriesString + stories.name + "\n"
        }
        let sixthString = NSAttributedString(string: auxStoriesString , attributes: secondAttributes)
        
         let sevenString = NSAttributedString(string: NSLocalizedString("Series", comment: "") + " \n ",attributes: firstAttributes)
        
        var auxSeriesString  = ""
        guard let arraySeries =  marvelHero?.series.items.prefix(3) else { return  }
        for series in arraySeries{
            auxSeriesString = auxSeriesString + series.name + "\n"
        }
        let eightString  = NSAttributedString(string: auxSeriesString , attributes: secondAttributes)

        
        
        firstString.append(secondString)
        firstString.append(thirdString)
        firstString.append(fourthString)
        firstString.append(fifthString)
        firstString.append(sixthString)
        firstString.append(sevenString)
        firstString.append(eightString)
        
        

        textView.attributedText = firstString;
        textView.textAlignment = NSTextAlignment.center
        textView.autocapitalizationType = UITextAutocapitalizationType.words
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isScrollEnabled = false
        
        self.scrollView.addSubview(textView)
    
    
    }
    
    func updateFavorite(){
        if(Favorites.isFavorite(idFavorite: marvelHero!.id)){
            //image favorite
            rightButton?.setImage(UIImage.init(named: "star-full.png"), for: .normal)
        }else{
            //image deselect
            rightButton?.setImage(UIImage.init(named: "star.png"), for: .normal)

        }
    }

    @objc func addToFavorite(sender: UIBarButtonItem) {
        if(Favorites.isFavorite(idFavorite: marvelHero!.id)){
            Favorites.removeFavorite(idFavorite: marvelHero!.id)
        }else{
            Favorites.addFavorite(idFavorite: marvelHero!.id)
        }
        updateFavorite()
    }
}
