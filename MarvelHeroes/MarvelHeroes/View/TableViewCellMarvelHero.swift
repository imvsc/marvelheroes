//
//  TableViewCellMarvelHero.swift
//  MarvelHeroes
//
//  Created by Isabel Couto on 18/04/2020.
//  Copyright © 2020 Isabel Couto. All rights reserved.
//

import UIKit

class TableViewCellMarvelHero: UITableViewCell {

    @IBOutlet weak var imageMarvelHero: UIImageView!
    @IBOutlet weak var nameMarvelHero: UILabel!
    @IBOutlet weak var imageLoader: UIActivityIndicatorView!
    var imageDownload :URLSessionDataTask?
    
    @IBOutlet weak var favoriteButton: UIButton!
    var marvelHeroCell:MarvelHeroe?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func addToFavorite(_ sender: Any) {
        if(Favorites.isFavorite(idFavorite: marvelHeroCell!.id)){
            Favorites.removeFavorite(idFavorite: marvelHeroCell!.id)
        }else{
            Favorites.addFavorite(idFavorite: marvelHeroCell!.id)
        }
        updateFavorite()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(marvelHero : MarvelHeroe){
        marvelHeroCell = marvelHero
        updateFavorite()
        nameMarvelHero.text = marvelHeroCell?.name;
        let url = URL(string: (marvelHeroCell?.thumbnail.path)! + "." + (marvelHeroCell?.thumbnail.extension)!)
       
        
        imageLoader.startAnimating()
        imageLoader.hidesWhenStopped = true;
        imageDownload?.cancel()
        imageDownload = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async() {    // execute on main thread
                self.imageMarvelHero.image = UIImage(data: data)
                self.imageLoader.stopAnimating()
                
            }
        }
        
        imageDownload?.resume()
       
    }
    
    func updateFavorite(){
        if(Favorites.isFavorite(idFavorite: marvelHeroCell!.id)){
            //image favorite
            favoriteButton.setImage(UIImage.init(named: "star-full.png"), for: .normal)
        }else{
            //image deselect
            favoriteButton.setImage(UIImage.init(named: "star.png"), for: .normal)

        }
    }

}
