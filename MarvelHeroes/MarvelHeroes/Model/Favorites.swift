//
//  Favorite.swift
//  MarvelHeroes
//
//  Created by Isabel Couto on 20/04/2020.
//  Copyright © 2020 Isabel Couto. All rights reserved.
//

import UIKit

class Favorites: NSObject {
    static func addFavorite(idFavorite:Int){
        let userDefaults = UserDefaults.standard;
        var arrayFavorites:[Int] =  (userDefaults.object(forKey: "FAVORITES") ?? []) as! [Int]
        arrayFavorites.append(idFavorite);
        userDefaults.set(arrayFavorites, forKey:  "FAVORITES");
        userDefaults.synchronize()
    }
    
    static func removeFavorite(idFavorite:Int){
        let userDefaults = UserDefaults.standard;
        var arrayFavorites:[Int]? = (userDefaults.object(forKey: "FAVORITES") ?? []) as! [Int]
        if let index = arrayFavorites?.firstIndex(of: idFavorite) {
            arrayFavorites?.remove(at: index)
        }
        userDefaults.set(arrayFavorites, forKey:  "FAVORITES");
        userDefaults.synchronize()
        
    }
    
    static func isFavorite(idFavorite:Int)->Bool{
        let userDefaults = UserDefaults.standard;
        let arrayFavorites:[Int]? = (userDefaults.object(forKey: "FAVORITES") ?? []) as! [Int]
        return arrayFavorites?.contains(idFavorite) ?? false
    }
}
