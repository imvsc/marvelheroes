//
//  MarvelHeroesTests.swift
//  MarvelHeroesTests
//
//  Created by Isabel Couto on 13/04/2020.
//  Copyright © 2020 Isabel Couto. All rights reserved.
//

import XCTest
@testable import MarvelHeroes

class MarvelHeroesTests: XCTestCase {

    override func setUp() {
        if let appDomain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
        UserDefaults.standard.synchronize()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testAddToFavourite() {
        Favorites.addFavorite(idFavorite: 1);
        
        XCTAssertEqual(Favorites.isFavorite(idFavorite: 1), true, "Error adding to favourite")
    }
    
    func testRemoveFromFavourites(){
        Favorites.addFavorite(idFavorite: 2);
        Favorites.removeFavorite(idFavorite: 2);
        XCTAssertEqual(Favorites.isFavorite(idFavorite: 2), false, "Error removing to favourite")

    }
    
    func testValidateIsFavouritesInFavourite(){
        Favorites.addFavorite(idFavorite: 3);
        XCTAssertEqual(Favorites.isFavorite(idFavorite: 3), true, "Error validating in favourite")
    }

    func testValidateIsFavouritesNotInFavourite(){
        XCTAssertEqual(Favorites.isFavorite(idFavorite: 4), false, "Error validating in favourite")
    }
    
    

}
